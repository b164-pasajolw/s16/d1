console.log("Hello World")



//Loops

//While Loop

/*
Syntax:
	while(expression/condition) {
		statement
	}

*/

//Create a function called displayMsgtoSelf()
	//-display a message to your past self in your console 10 times
	// Invoke the function 10 times


function displayMsgtoSelf(){
	console.log("Don't text her back.");
}

displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();
displayMsgtoSelf();

//while loop
//will allow us to repeat an action or an instruction as long as the condition is true

let count = 10;

while(count !== 0) {
	console.log("Don't text her back.")
	count--;
	}
/*
1st loop - Count 10
2nd - 9
3rd - 8 ....

9th loop - count 2
10th loop - count 1

If there is no decrementation the condition is always true, thus an infinite loop 

Infinite loop will run your code block FOREVER until you stop it./
or the browser will crash.

*/

let count1 = 5;

while(count1 !== 0) {
	console.log(count1);
	count1--;

}

//Miniactivity
// The ffg. while loop should display the numbers from 1-5 (in order);

let count2 = 1;

while(count2 !== 6) {
	console.log(count2);
	count2++;

}

//Do while loop

//A do-while loop is like a while loop. But do-while loops guarantee that the code while be executed atleast once.

/*
Syntax:
	do{
		statement
	} while(expression/condition)

*/

let doWhileCounter = 1;

do {
	console.log(doWhileCounter);
	doWhileCounter++
}while (doWhileCounter <= 20)

/*
let number = Number(prompt("Give me a number"));

do {
	console.log("Do while: " + number);

	//Increase the value of number by 1 after every iteration to stop the loop when it reaches 10
	//number - number + 1;
	number += 1;
} while (number < 10)
*/


// For Loop

/*
It consist of three parts:
1. initialization - value that will track the progression of the loop
2. expression/condition that will be evaluated which will determine whether the loop will run one more time.
3. finalExpression indicates how to advance the loop (++, --)
syntax:
	for (initialization; expression/condition; finalExpression)

*/

//loop from 0-20
for (let count = 0; count <= 20; count++) {
	console.log(count);
}

//For loops accessing array Items

let fruits = ["Apple", "Durian", "Kiwi", "Pineapple", "Mango", "Orange"];

console.log(fruits[2]);

//.length property is also a property of an array. that shows the total number of items in an array
console.log(fruits.length); //5 total number of items in array
// A more reliable way of checking the last item in an array;
//arrayName[arrayName.lenght-1]
console.log(fruits[fruits.length-1]); //orange

//show all the items in an array in the console using loops:

for(let index = 0; index < fruits.length; index++) {
	console.log(fruits[index]);
}

//Miniactivity

//Create an array with atleast 6 items as one of your favorite countries
//Display all items in the console Except for the last item

let countries = ["Japan", "Korea", "Singapore", "UAE", "Bahrain", "Qatar"];

for (let index = 0; index < countries.length-1; index++) {
	console.log(countries[index]);
	// (alternative, remove "-1" on .length above) console.log(countries[index-1]);
}


//for loops accessing elements of a string 

let myString = "alex";
//.length it is also a property used in strings
console.log(myString.length); //4 - characters

//Individual characters of a string can be access using it's index number. and it also start with 0 - nth number
console.log(myString[0]); //a - first character

//will create a loop that will printout the letters of the myString variable

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}

let myName = "Jane";

/*
	Create a loop that will print out the letters of the name individually and print out number 3 instead when the letter to be printed out is a vowel

*/

for(let  i = 0; i < myName.length; i++) {
	//if the character of your name is a vowel letter, display number "3"
	//console.log(myName[i]);
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		) {
		console.log(3);
	} else {
		//Print in all the non-vowel in the console
		console.log(myName[i])
	} 
}

//Continue and Break Statements.

//Continue Statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block ({})

//break statement is used to terminate the current loop once a match has been found

for(let count = 0; count <= 20; count++) {
	//if remainder is equal to 0, we will use the continue statement
	if (count % 2 === 0) {
		//Tells the code to continue to the next iteration
		//This ignores all statements located after the continue statement;
		continue;
	}
	//The current value of a number is printed out if the remainder is not equal to 0
	console.log("Continue and Break:" + count);


	//If the current value of count is greater than 10, then use the break if 
	if (count > 10) {
		break;
	}
}

//create a loop that will iterate based on the length of the string

let name = "alexandro";

for (let i = 0; i < name.length; i++) {
	console.log(name[i])

	//if the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration")
		continue;
	}

	if(name[i] == "d") {
		break;
	}
}